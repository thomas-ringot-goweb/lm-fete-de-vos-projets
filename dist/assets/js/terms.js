const $termsTrigger = document.querySelector(".m-terms-footer");

const $mainContent = document.querySelector(".container:first-of-type");
const $termsContent = document.querySelector("#terms");
const $pannelContent = document.querySelector(".u-pannel");

function toggleTerms(event) {
  event.preventDefault();
  $mainContent.classList.toggle("is-hidden");

  $pannelContent && $pannelContent.classList.toggle("is-hidden");

  $termsContent.classList.toggle("is-hidden");
}

