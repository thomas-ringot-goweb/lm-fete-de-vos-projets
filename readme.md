# Leroy Merlin - Jeu Concours 09/18

## Liens utiles

- [Ressources brutes (PSD, fonts, ...)](https://goweb59-my.sharepoint.com/:f:/g/personal/tr_goweb_fr/EtZVRKBnHzdPn9hlO8Rw81gBEa1U0mSP3HMT0Amg_69_Cw?e=5tMPoQ)
- DEMO:
  - DESKTOP:
    - [congratz](http://damaging-back.surge.sh/screens/desktop/congratz/)
    - [dotation](http://damaging-back.surge.sh/screens/desktop/dotation/)
    - [gift](http://damaging-back.surge.sh/screens/desktop/gift/)
    - [lost](http://damaging-back.surge.sh/screens/desktop/lost/)
    - [selection](http://damaging-back.surge.sh/screens/desktop/selection/)
    - [selection-end](http://damaging-back.surge.sh/screens/desktop/selection-end/)
    - [shop](http://damaging-back.surge.sh/screens/desktop/shop/)
  - MOBILE:
    - [congratz](http://damaging-back.surge.sh/screens/mobile/congratz/)
    - [dotation](http://damaging-back.surge.sh/screens/mobile/dotation/)
    - [gift](http://damaging-back.surge.sh/screens/mobile/gift/)
    - [lost](http://damaging-back.surge.sh/screens/mobile/lost/)
    - [selection](http://damaging-back.surge.sh/screens/mobile/selection/)
    - [selection-end](http://damaging-back.surge.sh/screens/mobile/selection-end/)
    - [shop](http://damaging-back.surge.sh/screens/mobile/shop/)

## How to

Compilateur less nécessaire, pour vscode cette [extension](https://marketplace.visualstudio.com/items?itemName=mrcrowl.easy-less) fait l'affaire. Puis:

```
git clone https://thomas-ringot-goweb@bitbucket.org/thomas-ringot-goweb/lm-fete-de-vos-projets.git
cd lm-fete-de-vos-projets
# ouvrir n'importe quel fichier dans dist
```

## Explications

- Un fichier html par page et par device (se trouvent dans `dist/screens/`).
- Deux fichier css prod (`dist/assets/css/mobile.css` et `dist/assets/css/mobile.css`)
- Obtenu en compilant `styles/shared/default-<mobile|mobile>.less` et `styles/shared/default.less`
